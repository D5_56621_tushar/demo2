create table Movie(
    movie_id INTEGER PRIMARY KEY auto_increment,
    movie_title VARCHAR(50),
    movie_release_date DATE,
    movie_time FLOAT,
    director_name VARCHAR(100)
);
