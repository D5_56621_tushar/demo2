const express = require("express")
const cors = require("cors")
const db = require("./db")
const app = express()
const utils = require("./utils")

app.use(cors("*"))
app.use(express.json())

app.get('/movie/:movie_title', (request, response) => {
    const { movie_title } = request.params

    const query = `SELECT * FROM Movie WHERE movie_title = '${movie_title}'`

    db.execute(query, (error, result) => {
        response.send(utils.createResult(error, result))
    })
})

app.post('/movie', (request, response) => {
    const {  movie_title, movie_release_date ,movie_time , director_name } = request.body

    const query = `INSERT INTO Movie VALUE(default, '${movie_title}', '${movie_release_date}', ${movie_time}, '${director_name}')`

    db.execute(query, (error, result) => {
        response.send(utils.createResult(error, result))
    })
})

app.put('/movie/:movie_title', (request, response) => {
    const movie_title = request.params
    const { movie_release_date, movie_time } = request.body

    const query = `UPDATE Movie SET movie_release_date = '${movie_release_date}', movie_time = '${movie_time}' WHERE movie_title = '${movie_title}'`

    db.execute(query, (error, result) => {
        response.send(utils.createResult(error, result))
    })
})

app.delete('/movie/:movie_title', (request, response) => {
    const { movie_title } = request.params

    const query = `DELETE FROM Movie WHERE movie_title = '${movie_title}'`

    db.execute(query, (error, result) => {
        response.send(utils.createResult(error, result))
    })
})

app.listen(4000, () => {
    console.log("server started on 4000")
})