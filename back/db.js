const mysql = require("mysql2")

const pool = mysql.createPool( {
    host: "dbcon",
    user: 'root',
    password: 'root',
    database: 'movies',
    waitForConnections: true,
    connectionLimit: 10,
    queueLimit: 0
});
module.exports = pool